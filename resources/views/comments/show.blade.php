@extends('layouts.app')


@section('content')

<div class="container">
        <div class="row  mt-5 mb-3 justify-content-end">
            <div class="d-flex flex-column">
                <a class="btn btn-dark" href="{{route('posts.index')}}" role="button">Back</a>
            </div>
        </div>

        <div class="row mt-5 mb-3">
                @foreach($posts as $post)
                <div class="col-md-4">
                    <div class="card mt-3 mb-3">
                        <img src="{{$post->img_path}}" class="card-img-top img-fluid">
                        <div class="card-body">
                            <div class="card-title">
                                <h3 class="card-title">Author_id: {{$post->author->name}} </h3>
                            </div>
                            <a href="{{route('posts.comments',$post->id)}}" class="btn btn-primary" role="button">Add Comments</a>  
                            <div class="card-header">
                                Comments
                            </div>
                            
                            <ul class="list-group list-group-flush">
                                <blockquote class="blockquote mb-0">
                                    @foreach($post->comments as $comment)
                                        <li class="list-group-item">
                                            {{$comment->body}}<footer class="blockquote-footer">{{$comment->author->name}}</footer>
                                        </li>
                                    @endforeach
                                </blockquote>
                            </ul>
                        </div>
                    </div>
                    <!-- <li><a href="{{route('posts.show',$post->id)}}"> {{$post->title}} </a></li> -->
                
                </div>
                @endforeach
        </div>
    <!-- @foreach($posts as $post)

    <div class="image-container">
        <img height=200 width=300 src="{{$post->img_path}}" alt="not found">
    </div>
    <div> Author_id: {{$post->author_id}} </div>
    @endforeach -->
@endsection


@yield('footer')