<?php
namespace App\Http\Requests;
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

use App\Models\Post;
use App\Models\Comment;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreatePostRequest;
use Illuminate\Http\Request;

use Illuminate\Database\Eloquent\Model;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
   {
       $this->middleware('auth',['except'=>['index']]);
   }
    public function index()
    {   
        // $posts = Post::all();
        if(Auth::user()){
            $user = Auth::user()->id;
            $posts = Post::with('author')->orderBy('id','desc')->get();
            return view('posts.index',compact('posts'))->with('user',$user);
        }
        else{
            $posts = Post::with('author')->orderBy('id','desc')->get();
            return view('posts.home',compact('posts'));
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $user = $user->id;


        if($file = $request->file('file')){
            $name = $file->getClientOriginalName();

            $path = $request->file('file')->storeAs(
                'photos',$name
            );
        }

        $post = new Post;
        $post->img_path = $path;
        $post->author_id = $user;

        $post->save();
        return redirect('/posts');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Posts  $posts
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $posts = Post::with('author')->Where('author_id',$id)->orderBy('id','desc')->get();
        
        return view('posts.show',compact('posts'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Posts  $posts
     * @return \Illuminate\Http\Response
     */
    public function edit(Posts $posts)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Posts  $posts
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Posts $posts)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Posts  $posts
     * @return \Illuminate\Http\Response
     */
    public function destroy(Posts $posts)
    {
        //
    }

    public function myposts()
    {
        # code...
    }
}
