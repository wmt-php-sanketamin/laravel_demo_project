<?php

use Illuminate\Support\Facades\Route;
use App\Models\User;
use App\Models\Post;
use App\Models\Comment;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('posts', 'App\Http\Controllers\PostsController');
Route::resource('comments', 'App\Http\Controllers\CommentsController');

Route::get('/posts/create', 'App\Http\Controllers\PostsController@create')->middleware('auth')->name('posts.create');

Auth::routes();
// Route::get('/all/posts',function(){
//     $posts = Post::orderBy('id','desc')->get();
//     $comments = Comment::all();
//     return view('posts.index',compact('posts','comments'));
// });


// Route::get('user/{id}/posts', function ($user){
//     $posts = Post::with('author')->Where('author_id',$user)->orderBy('id','desc')->get();
    
//         return view('posts.show',compact('posts'));
// })->name('user.posts')->middleware('auth');



Route::get('posts/{id}/comments', function ($post_id){

    $posts = Post::with('author','comments')->Where('id',$post_id)->get();
    $comments = Comment::with('author')->where('post_id',$post_id)->get();

    // return $comments;
        return view('comments.create',compact('posts','comments'));

})->name('posts.comments')->middleware('auth');

Route::get('/posts/{id}/data', function($post_id){
            
    $posts = Post::with('author','comments')->where('id',$post_id)->get();
    
    return view('comments.show',compact('posts'));

})->name('posts.data')->middleware('auth');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
