<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;
    protected $fillable = [
        'body',
        'author_id',
        'post_id'
    ];
    public function author()
    {
        return $this->belongsTo('App\Models\User', 'author_id');
    }
  
    public function post()
    {
        return $this->belongsTo('App\Models\Post', 'post_id');
    }
}
