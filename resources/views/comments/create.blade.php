@extends('layouts.app')


@section('content')
        <div class="container">
            <div class="row  mt-5 mb-3 justify-content-end">
                <div class="d-flex flex-column">
                    <a class="btn btn-dark" href="{{route('posts.index')}}" role="button">Back</a>
                </div>
            </div>
            <div class="row mt-5 mb-3">
                @foreach($posts as $post)
                    <div class="col-md-4">
                        <div class="card mt-3 mb-3">
                            <img src="{{$post->img_path}}" class="card-img-top img-fluid">
                            <div class="card-body">
                                <div class="card-title">
                                    <h3 class="card-title">Author: {{$post->author->name}} </h3>
                                </div> 
                                <div class="card-header">
                                    Comments
                                </div>
                                <blockquote class="blockquote mb-0">
                                    <p>
                                        <form method="post" action="/comments">
                                        @csrf
                                            <input type="text" name="body" placeholder="Enter Comment">
                                            <input type="hidden" name="post_id" value="{{ $post->id }}">
                                            <input type="submit" class = "btn btn-primary" name="submit">
                                        </form>
                                        <!-- {!! Form::open(['method'=>'POST','action'=>'App\Http\Controllers\CommentsController@store']) !!}
                                        @csrf
                                        {!! Form::text('body',null, ['class'=>'form-control']) !!}
                                        {!! Form::hidden('post_id', '{{$post->author_id}}') !!}
                                        {!! Form::submit('Submit',['class'=>'btn btn-primary mt-3']) !!}

                                        {!! Form::close() !!} -->
                                    </p>
                                    <ul class="list-group list-group-flush">
                                    @foreach($comments as $comment)
                                        <li class="list-group-item">
                                            {{$comment->body}}<footer class="blockquote-footer">{{$comment->author->name}}</footer>
                                        </li>
                                    @endforeach
                                </blockquote>
                            </ul>
                                </blockquote>
                            </div>
                        </div>
                        <!-- <li><a href="{{route('posts.show',$post->id)}}"> {{$post->title}} </a></li> -->
                    </div>
                @endforeach    
            </div>
        </div>
@endsection


@yield('footer')