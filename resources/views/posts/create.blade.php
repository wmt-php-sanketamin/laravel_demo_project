@extends('layouts.app')


@section('content')

    <h1>Create Post: </h1>

    {!! Form::open(['method'=>'POST','action'=>'App\Http\Controllers\PostsController@store','files'=>true]) !!}
    @csrf
    <div class="form-group">

        {!! Form::label('select File','Select Image: ') !!}
        {!! Form::file('file',null, ['class'=>'form-control']) !!}

        {!! Form::submit('Create Post',['class'=>'btn btn-primary']) !!}
    </div>
    {!! Form::close() !!}

@endsection


@yield('footer')
