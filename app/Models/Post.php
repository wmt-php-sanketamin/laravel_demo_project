<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    // public $directory = "./images/";

    use HasFactory;
    protected $fillable = [
        'img_path'
    ];
    public function comments()
    {
        return $this->hasMany('App\Models\Comment', 'post_id');
    }

    public function author()
    {
        return $this->belongsTo('App\Models\User', 'author_id');
    }
    // public function getImgPathAttribute($value){
    //     return $this->directory . $value;
    // }
    public function getImgPathAttribute()
    {
        if ($this->attributes['img_path'] !== null) {
            // return config('appConstant.GOOGLE_UPLOAD_PATH') . $this->attributes['img_path'];
            return url('/storage/' . $this->attributes['img_path']);
            // print_r(url('/storage/' . $this->attributes['img_path']))
        }
        return null;
    }

}
